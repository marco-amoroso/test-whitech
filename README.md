This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Test instructions
https://github.com/whitechdevs/reactjs-test

## Notes
App was tested on Mac's:
* Chrome 69.0.3497.100
* Safari 12.0 (12606.2.11)
* Firefox 62.0.2

### TODO list
* Cleanup webpack and script files
* Add loading state while fetching products
* Add loading states while loading product images
* Make the elements on the page more accessible
* Add Storybook or similar to showcase the React components in isolation
* Move the API urls into an .env file to be able to switch url depending on what environment we are using (`test`, `staging`, `production`, etc.)
* Add ability to navigate the product pages via keyboard (left/right arrow etc.)

### CSS
I have used different css solutions in the past (`glamour` or `css/sass` via webpack loaders, etc.), I picked `styled-components` for this test.

The styles are not pixel perfect due to time restriction, I focused mostly on the Pagination flow work. I will need to discuss with the Designer about font-families used, colors, spacing, etc. I got them using a color picker and colors are most likely off.

I decided to leave the styles for the `dropdown` select element alone, it is rendering as the browser default. In my opinion dropdowns should be touched as little as possible, especially to improve the usability for Mobile users or users with accessibility problems.

### Flow
I added a type checking with `flow`. It is not perfect but it is better than nothing. I found it helpful in the past finding bugs before they hit production.

### CI/CD
Project is hosted on Gitlab, I have added a simple `.gitlab-ci.yml` file to simply run `yarn test` on each push.

### Further cleanup needed before production
I did ejected from the `create-react-app` project and all the default files for webpack/jest/babel etc. were added to the project. I did eject the project to be able to customise the repo (`eslint`, `flow`, etc.). A proper cleanup would be needed before being production-ready.

### Testing
I used `enzyme` and `jest` to run unit tests and `snapshot` tests. I find the snapshots particularly helpful during Pull requests.

### How to run
```
yarn
yarn test
yarn start
```

### Screenshots

Mobile | Tablet | Desktop
--- | --- | ---
![](screenshots/mobile.png) | ![](screenshots/tablet.png) | ![](screenshots/desktop.png)
