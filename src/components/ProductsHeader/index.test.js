import React from 'react'
import ProductsHeader from '../ProductsHeader'
import renderer from 'react-test-renderer'

const defaultProps = {
  total: 10,
  onSizeChange: () => {},
}

describe('<ProductListings />', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<ProductsHeader {...defaultProps} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders correctly with missing props', () => {
    const tree = renderer
      .create(<ProductsHeader />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
