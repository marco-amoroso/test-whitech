import React from 'react'
import ProductListings from '../ProductListings'
import mockProducts from '../../utils/mock'
import renderer from 'react-test-renderer'

describe('<ProductListings />', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<ProductListings products={mockProducts(5)} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('renders correctly with missing props', () => {
    const tree = renderer
      .create(<ProductListings />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
