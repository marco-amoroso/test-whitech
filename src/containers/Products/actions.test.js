import { fetchProductsLoading, fetchProductsSuccess, fetchProductsError, selectPage, selectSize } from './actions'
import { FETCH_PRODUCTS_LOADING, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR, SELECT_PAGE, SELECT_SIZE } from './constants'
import mockProducts from '../../utils/mock'

describe('actions', () => {
  describe('Product Fetching', () => {
    it('should create an action to load products', () => {
      const expectedAction = {
        type: FETCH_PRODUCTS_LOADING,
      }
      expect(fetchProductsLoading()).toEqual(expectedAction)
    })
    it('should create an action to add products', () => {
      const products = mockProducts(2)
      const expectedAction = {
        type: FETCH_PRODUCTS_SUCCESS,
        payload: {
          products,
        }
      }
      expect(fetchProductsSuccess(products)).toEqual(expectedAction)
    })
    it('should create an action to flag an error fetching products', () => {
      const expectedAction = {
        type: FETCH_PRODUCTS_ERROR,
      }
      expect(fetchProductsError()).toEqual(expectedAction)
    })
  })
  describe('Product Page Update', () => {
    it('should create an action to update current page', () => {
      const expectedAction = {
        type: SELECT_PAGE,
        payload: {
          currentPage: 3,
        }
      }
      expect(selectPage(3)).toEqual(expectedAction)
    })
  })
  describe('Product Size Update', () => {
    it('should create an action to update page size', () => {
      const expectedAction = {
        type: SELECT_SIZE,
        payload: {
          size: 5,
        }
      }
      expect(selectSize(5)).toEqual(expectedAction)
    })
  })    
})
