// @flow
import React from 'react'
import styled from 'styled-components'
import ProductListings from '../../components/ProductListings'
import type { ProductsType } from '../../components/ProductListings'
import ProductsHeader from '../../components/ProductsHeader'
import Pagination from '../../components/Pagination'
import enhance from './enhance'
import spacings from '../../styles/spacings'
import colors from '../../styles/colors'

const Wrapper = styled.main`
  background-color: ${colors.BG};
  min-height: 100vh;
`

const Inner = styled.section`
  padding: ${spacings.MEDIUM} ${spacings.LARGE};
`

type Props = {
  products: ProductsType,
  size: number,
  currentPage: number,
  totalPages: number,
  onPageClick: () => () => {},
  onSizeChange: () => {},
}

const Products = ({ products, size, currentPage, totalPages, onPageClick, onSizeChange }: Props) => {
  if (!Array.isArray(products) || products.length === 0) {
    return null
  }
  
  const index = (currentPage - 1) * size
  const showProducts = products.slice(index, index + size)

  return (
    <Wrapper>
      <Inner>
        <ProductsHeader total={products.length} onSizeChange={onSizeChange} />
        <ProductListings products={showProducts} />
        <Pagination
          currentPage={currentPage}
          totalPages={totalPages}
          onPageClick={onPageClick}
        />
      </Inner>
    </Wrapper>
  )
}

export { Products }
export default enhance(Products)
