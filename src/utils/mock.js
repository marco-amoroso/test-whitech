import times from 'lodash.times'

const mockProducts = (multiplier) => times(multiplier, (index) => ({
  id: index,
  product_name: `Test Product ${index}`,
  description: `Test Description ${index}`,
  price: `$${10 * index + 1}`,
  product_image: 'https://test.image.jpg',
}))

export default mockProducts
