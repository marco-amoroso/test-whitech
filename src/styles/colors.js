export default {
  BG: '#f6f6f6',
  WHITE: '#fff',
  BORDER: '#e5e5e5',
  DARK_BORDER: '#000000',
  HEADING: '#5d5d5d',
}
